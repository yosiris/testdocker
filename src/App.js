import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Dockerized React App Example
        </p>
        <a
          className="App-link"
          href="https://gitlab.com/yosiris"
          target="_blank"
          rel="noopener noreferrer"
        >
          With ❤️ by Yosiris
        </a>
      </header>
    </div>
  );
}

export default App;
