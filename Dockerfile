FROM node:18.16.0-alpine3.17 as builder
WORKDIR /app
COPY . .
RUN npm ci
RUN npm run build

FROM nginx:1.24.0-alpine3.17
ENV NODE_ENV production
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
